var config = require('../config')
const electron = require('electron');
var bonjour = require('bonjour')();
var os = require('os')
var ip = require('ip')

ifaces = os.networkInterfaces();

// default host ip address
host_ip = '127.0.0.1';

const {app} = electron;
const {BrowserWindow} = electron;

service_name = process.env.USER + '@' + os.hostname();

// start advertising the machines presence
var service = bonjour.publish({ name:service_name, type:"http", port:3000, host: ip.address() });

let win;
function createWindow(){
    win = new BrowserWindow({
        darkTheme: true, // Foreces Dark Theme (GTK+3)
        title: config.APP_WINDOW_TITLE,
        titleBarStyle: 'hidden-inset',
        width: 800,
        height: 600,
        minWidth: 800-40,
        minHeight: 600-50,
        icon: config.APP_ICON
    });

    win.setMenu(null);
    win.loadURL(config.WINDOW_MAIN);

    win.on('closed', () => {
        service.stop();
        win = null;
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if(process.platform !== 'darwin'){
        /* storage.clear(function(error) {
              if (error) throw error;
        });*/
        app.quit();
    }
});

app.on('activate', () => {
    if (win === null){
        createWindow();
    }
});
