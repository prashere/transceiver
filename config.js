var APP_NAME = "Transceiver"
var APP_VERSION = require('./package.json').version

module.exports = {
    WINDOW_MAIN: 'file://' + __dirname + '/renderer/index.html',
    APP_WINDOW_TITLE: APP_NAME + ' - ' +  APP_VERSION,
    APP_ICON: __dirname + '/static/app_icon.png',
    NOTIFICATION: true // notifications enabled by default
}
