function simulateClick(e) {
    if (e.keyCode == 13){
        document.getElementById("send").click();
    }
}

function openInExternalApp(e) {
    if (e.target.tagName.toLowerCase() === "a") {
        e.preventDefault();
        platform = require('os').platform();
        if (platform === "linux"){
            require('child_process').exec("xdg-open " + e.target.getAttribute("href"));
        }
        /* use shell in case of windows and mac osx */
        else if (platform === "win32" || platform === "darwin"){
            require('electron').shell.openExternal(e.target.getAttribute("href"));
        }
    }
} 
