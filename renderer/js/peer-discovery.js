var config = require ('../config')

// initialize the json storage object
const storage = require('electron-json-storage')

// initiate the bonjour object
var bonjour = require("bonjour")();

// create a service discovery browser
var service_browser = bonjour.find({ type: "http" });

// dictionary to hold the socket of 
// all connected sockets
var connected_clients = {};


/* callback to be executed
 * when a peer comes online */
function onUp(service) {
    //console.log(service.name+' connected')
    // store the user host ip as key with their name
    storage.set(service.host, { name: service.name }, function(error){
        if (error) throw error;
    });
   
    // li item as a peer container
    // contains two items image and span
    li_node = document.createElement("li");
    li_node.id = service.host;
    li_node.className = "list-group-item clickable animated bounceInRight";
    li_node.addEventListener('click', connectListener, false);

    // device image to display along the peer
    img_node = document.createElement("img");
    img_node.className = "img-circle media-object pull-left";
    img_node.src = "images/laptop.png";
    img_node.width = img_node.height = 32;

    // span element with strong and p elements
    span_node = document.createElement("span");
    span_node.className = "clickable";

    peer_id = document.createElement("strong");
    peer_id.textContent = service.name;
    peer_id.className = "clickable";

    p_node = document.createElement("p");
    p_node.textContent = "Available";
    p_node.className = "clickable";

    span_node.appendChild(peer_id);
    span_node.appendChild(p_node);
    
    li_node.appendChild(img_node);
    li_node.appendChild(span_node);

    document.getElementById("peers-list").appendChild(li_node);

    // send system notification on peer connect if enabled
    if (Notification.permission === "granted" && config.NOTIFICATION === true){
        connected_notify = new Notification(service.name+" Connected", { body: "Peer Discovery - Transceiver" })
    }
};

function onDown(service){
    console.log(service.name+' disconnected');
    //connected_clients[service.host].close();

    // clear the footprint of this peer from json storage.
    storage.has(service.host, function(error, haskey){
        if (error) throw error;
        if (haskey) {
            storage.remove(service.host, function(error){
                if (error) throw error;
            });
            console.log(service.host + " removed");
        }
    });

    // animates the peer element from the left
    // panel indicating the peer has left.
    li_node = document.getElementById(service.host);
    li_node.classList.add("bounceOutRight");

    // bring back the overlay if the active user left
    // because the you cannot send anything to them
    if (li_node.classList.contains("active")){
        document.getElementById("inner-box").style.display = "block";
        document.getElementById("peer-label").textContent = "peer name";
    }

    // once the animation is over, remove the
    // peer element from DOM and send a notification to
    // user if enabled.
    setTimeout(function () {
        li_node.remove();
        if (Notification.permission === "granted" && config.NOTIFICATION === true){
            disconnected_notify = new Notification(service.name+" Disconnected", { body: "Peer Discovery - Transceiver" })
        }
    }, 1000);
    
    // close any existing open socket with respect
    // to this peer and set the value to undefined.
    connected_clients[service.host] = undefined;
};

service_browser.on('up', onUp);
service_browser.on('down', onDown);
