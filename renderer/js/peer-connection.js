// import the filesystem library
var fs = require('fs');

// constant PORT number to serve
// and listen to.
var PORT = 8888;

/** server logic starts here **/

// creating a server socket on 
// constant port.
var server_socket = require('socket.io')(PORT);

// receive message from client
// socket on connection.
server_socket.on('connection', function(socket){
    socket.on('init', function(msg) {
        console.log(msg);
    });

    socket.on('message', function(data) {

        data.value = anchorme.js(data.value);

        on_user_div = document.getElementsByClassName("on-user")[0];
        no_user_div = document.getElementsByClassName("no-user")[0];
        no_message_div = document.getElementById("no-message");
        message_display_div = document.getElementById("message-display");

        tr_element = document.createElement("tr");
        sender_element = document.createElement("td");
        message_element = document.createElement("td");

        if (data.value.search("<a href") !== -1){
            message_element.addEventListener('click', openInExternalApp, true);
        }

        message_element.innerHTML = data.value;

        sender_ip = socket.client.conn.remoteAddress.split(":").pop();
        storage.get(sender_ip, function (error, peer) {
            sender_element.textContent = peer.name;
        });
 
        if (no_message_div.style.display === "block"){
            no_message_div.style.display = "none";
            message_display_div.style.display = "block";
        }

        tr_element.appendChild(sender_element);
        tr_element.appendChild(message_element);

        document.getElementById("table-body").appendChild(tr_element);
    });

    socket.on('document', function(data) {
       fs.writeFile('pymesh.webm', data.buffer, function (err) {
           if (err) throw err;
           console.log('It\'s saved!');
       })
    });
});

/** server logic ends here **/

/** client logic starts here **/

var client_socket = null;

// perform the following operation
// when the user clicks on one of
// the peer from the list.
function connectListener(e) {

    //console.log(e.target.tagName.toLowerCase());
    switch(e.target.tagName.toLowerCase()) {
        case "li":
            REMOTE_HOST = e.target.getAttribute("id");
            storage.get(REMOTE_HOST, function(error, data){
                if (error) throw error;
                // else use the data to change the header
                document.getElementById("peer-label").textContent = data.name;
            });
            break;
        case "strong":
            REMOTE_HOST = e.target.parentNode.parentNode.id;
            document.getElementById("peer-label").textContent = e.target.textContent;
            break;
        case "p":
            REMOTE_HOST = e.target.parentNode.parentNode.id;
            document.getElementById("peer-label").textContent = e.target.previousElementSibling.textContent;
            break;
        case "img":
            REMOTE_HOST = e.target.parentNode.id;
            document.getElementById("peer-label").textContent = e.target.nextElementSibling.children[0].textContent;
            break;
        case "span":
            REMOTE_HOST = e.target.parentNode.id;
            document.getElementById("peer-label").textContent = e.target.children[0].textContent;
            break;
    }

    // remove active class from all the class
    // then add it on the class that is being clicked
    lis = document.getElementById("peers-list").getElementsByTagName("li");
    for (i=0; i<lis.length; i++){
        if (lis[i].id !== REMOTE_HOST){
            lis[i].classList.remove("active");
        }
    }

    document.getElementById(REMOTE_HOST).classList.add("active");
    document.getElementsByClassName("on-user")[0].style.display = "block";
 
    CONNECT_TO_ENDPOINT = "http://" + REMOTE_HOST + ":" + PORT.toString();

    /* first time, so expect client_socket to be null */
    if (client_socket === null){
        client_socket = require('socket.io-client')(CONNECT_TO_ENDPOINT);
        client_socket.on('connect', function() {
            client_socket.emit('init', 'ping');
        });
        connected_clients[REMOTE_HOST] = client_socket;
    } else {
        if (client_socket.io.uri !== CONNECT_TO_ENDPOINT){
            if (connected_clients[REMOTE_HOST] === undefined){
                client_socket = require('socket.io-client')(CONNECT_TO_ENDPOINT);
                client_socket.on('connect', function() {
                    client_socket.emit('init', 'ping');
                });
                connected_clients[REMOTE_HOST] = client_socket;
            } else {
                client_socket = connected_clients[REMOTE_HOST];
            }
        } else {
            console.log("You are clicking on the same element");
        }
    }
    document.getElementById("inner-box").style.display = "none";
}

function sendMessage(textOrURL) {
    if (textOrURL.length !== 0){
        client_socket.emit('message', { value: textOrURL });
        document.getElementById("url").value = "";
    }
}

// function to read and send image
// to other peer
/**function sendFile(image_file){
    fs.readFile(image_file, function(err, buf) {
        client_socket.emit('document', {image: image_file, buffer: buf});
    });
}**/
